#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

mkdir -p ${PWD}/csi
WORKDIR=${PWD}/csi
cd ${WORKDIR}

# Get kubeconfig file
gsutil cp gs://${PROJECT_ID}/kubeconfig/kubeconfig_${EKS_CLUSTER} ${WORKDIR}/kubeconfig_${EKS_CLUSTER}

# Get the eksctl tool
echo "Install eksctl"
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_linux_amd64.tar.gz" | tar xz -C ${WORKDIR}
./eksctl version

# Create OIDC provider for cluster
echo "Create OIDC provider for the cluster"
./eksctl utils associate-iam-oidc-provider \
    --cluster=${EKS_CLUSTER} \
    --approve

# Create CSI Driver IAM role
echo "Create CSI drive IAM role"
./eksctl create iamserviceaccount \
    --name ebs-csi-controller-sa \
    --namespace kube-system \
    --cluster ${EKS_CLUSTER} \
    --attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy \
    --approve \
    --role-only \
    --role-name AmazonEKS_EBS_CSI_DriverRole_${EKS_CLUSTER}

# Install CSI addon
echo "Install EBS CSI addon"
echo "AWS Account ID: ${AWS_ACCOUNT_ID}"
./eksctl create addon --name aws-ebs-csi-driver \
    --cluster ${EKS_CLUSTER} \
    --service-account-role-arn arn:aws:iam::${AWS_ACCOUNT_ID}:role/AmazonEKS_EBS_CSI_DriverRole_${EKS_CLUSTER} \
    --force